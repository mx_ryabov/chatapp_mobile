import {
    UserActionTypes,
    UserState,
    START_FETCH,
    FETCH_ERROR,
    AUTH_SUCCESS,
    GET_USER_SUCCESS,
    VK_AUTH_SUCCESS,
    GET_ME_SUCCESS,
    REGISTER_USER_SUCCESS,
  } from "./types";
  
  const initialState: UserState = {
    fetching: false,
    me: { login: "", firstName: "", _id: "", secondName: "" },
    contacts: [],
    error: ""
  };
  
  export function userReducer( 
      state = initialState, 
      action: UserActionTypes
  ): UserState {

      switch (action.type) {
        case START_FETCH:
            return {
                ...state,
                fetching: true
            };

        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                fetching: false
            }

        case GET_ME_SUCCESS:
            return {
                ...state,
                me: action.payload,
                fetching: false
            }

        case VK_AUTH_SUCCESS:
            return {
                ...state,
                me: action.payload,
                fetching: false
            }

        case AUTH_SUCCESS:
            return {
                ...state,
                me: action.payload,
                fetching: false
            };

        case GET_USER_SUCCESS:
            return {
                ...state,
                contacts: action.payload,
                fetching: false
            };

        case FETCH_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.error
            };


        default:
        return state;
      }
  }