import {
    START_FETCH, UserState, UserActionTypes, StartFetchAction, 
    AUTH_SUCCESS, FETCH_ERROR, IUser, GET_USER_SUCCESS, GET_ME_SUCCESS, REGISTER_USER_SUCCESS
} from './types';
import { getRequest, postRequest } from "../../helpers";
import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { AsyncStorage, Alert } from 'react-native';


export const signUp = (
        login:string, password:string, firstName:string, 
        secondName:string, done: (res) => void
    ):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await postRequest("/user/sign_up", {login, password, firstName, secondName});
        
        const success:UserActionTypes = {type: REGISTER_USER_SUCCESS, payload: res};
        dispatch(success);
        done(res);
        
    } catch(e) {
        Alert.alert("Ошибка регистрации", "Пользователь с таким логином уже существует!");       
        const error:UserActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }    
}

export const logIn = (login:string, password:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await postRequest("/user/login", {login, password});
        await AsyncStorage.setItem("token", res.token);
        
        const success:UserActionTypes = {type: AUTH_SUCCESS, payload: res};
        dispatch(success);
        
    } catch(e) {
        console.log(e);
        
        Alert.alert("Неверные логин или пароль", "Попробуйте снова!")        
        const error:UserActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }    
}

export const vkLogIn = (code:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await postRequest("/user/vk_auth", {code});
        await AsyncStorage.setItem("token", res.token);
        
        const success:UserActionTypes = {type: AUTH_SUCCESS, payload: res.user};
        dispatch(success);
        
    } catch(e) {
        Alert.alert("Неверные логин или пароль", "Попробуйте снова!")        
        const error:UserActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }    
}

export const getUser = (_id?:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:any = await getRequest("/user/get", {});
        
        const success_users:UserActionTypes = {type: GET_USER_SUCCESS, payload: res.users};
        const success_me: UserActionTypes = {type: GET_ME_SUCCESS, payload: res.me};
        dispatch(success_users);
        dispatch(success_me);
    } catch(e) {
        const error:UserActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    } 
}