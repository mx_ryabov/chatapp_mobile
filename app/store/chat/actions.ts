import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { START_FETCH, StartFetchAction, ChatActionTypes, FETCH_ERROR, IMessage, IChat, GET_CURRENT_CHAT_SUCCESS, GET_CHAT_LIST_SUCCESS, GET_MESSAGES_SUCCESS, SEND_MESSAGE_SUCCESS, CLEAR_CHAT_SCREEN } from "./types";
import { getRequest, postRequest } from "../../helpers";

export const clearChat = ():ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    dispatch({type: CLEAR_CHAT_SCREEN});
}

// message: string, addressee_id: ObjectId
export const sendMessage = (message:string, addressee_id:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:IMessage = await postRequest("/chat/message/send", {message, addressee_id});
        
        const success:ChatActionTypes = {type: SEND_MESSAGE_SUCCESS, payload: res};
        dispatch(success);
    } catch(e) {
        const error:ChatActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }
}

// message: string, addressee_id: ObjectId
export const getMessages = (addressee_id:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);    
    
    try {
        const res:IMessage[] = await getRequest("/chat/message/get", {addressee_id});
        
        const success:ChatActionTypes = {type: GET_MESSAGES_SUCCESS, payload: res};
        dispatch(success);
    } catch(e) {
        const error:ChatActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }
}

// messages: IMessage[]
export const setMessageSocket = (messages:IMessage[]):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    dispatch({type: GET_MESSAGES_SUCCESS, payload: messages});
}

// message: IMessage
export const setNewMessageSocket = (message:IMessage):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    dispatch({type: SEND_MESSAGE_SUCCESS, payload: message});
}

// addressee_id?: ObjectId, chat_id?: ObjectId
export const getChat = (addressee_id?:string, chat_id?:string):ThunkAction<Promise<void>, {}, {}, AnyAction> => async (dispatch:ThunkDispatch<{}, {}, AnyAction>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);

    try {
        if (addressee_id || chat_id) {
            const res:IChat = await getRequest("/chat/get", {addressee_id, chat_id});

            const success:ChatActionTypes = {type: GET_CURRENT_CHAT_SUCCESS, payload: res};
            dispatch(success);
        } else {
            const res:IChat[] = await getRequest("/chat/get", {});

            const success:ChatActionTypes = {type: GET_CHAT_LIST_SUCCESS, payload: res};
            dispatch(success);
        }
    } catch(e) {
        const error:ChatActionTypes = {type: FETCH_ERROR, error: e };
        dispatch(error);
    }
};