import { IUser } from "../user/types";

export interface IMessage {
    text: string;
    date: string;
    from: string;
    _id: string;
}

export interface IChat {
    users: IUser[];
    messages: IMessage[];
    contact: IUser;
    _id: string;
}

export interface ChatState {
    fetching: boolean;
    chat_list: IChat[];
    current_chat: IChat;
    error: Object | undefined;
}


export const START_FETCH = "START_FETCH";
export const FETCH_ERROR = "FETCH_ERROR";

// success
export const GET_CHAT_LIST_SUCCESS = "GET_CHAT_LIST_SUCCESS";
export const GET_CURRENT_CHAT_SUCCESS = "GET_CURRENT_CHAT_SUCCESS";
export const GET_MESSAGES_SUCCESS = "GET_MESSAGES_SUCCESS";
export const SEND_MESSAGE_SUCCESS = "SEND_MESSAGE_SUCCESS";
export const CLEAR_CHAT_SCREEN = "CLEAR_CHAT_SCREEN";


export interface StartFetchAction {
    type: typeof START_FETCH;
    payload?: ChatState
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: ChatState;
    error?: any;
}

interface ClearChatScreen {
    type: typeof CLEAR_CHAT_SCREEN
}


interface GetChatListSuccess {
    type: typeof GET_CHAT_LIST_SUCCESS;
    payload: IChat[];
}

interface GetCurrentChatSuccess {
    type: typeof GET_CURRENT_CHAT_SUCCESS;
    payload: IChat;
}

interface GetMessagesSuccess {
    type: typeof GET_MESSAGES_SUCCESS;
    payload: IMessage[]
}

interface SendMessageSuccess {
    type: typeof SEND_MESSAGE_SUCCESS;
    payload: IMessage
}


export type ChatActionTypes = StartFetchAction 
                              | FetchActionError
                              | ClearChatScreen
                              | GetChatListSuccess
                              | GetMessagesSuccess
                              | SendMessageSuccess
                              | GetCurrentChatSuccess;