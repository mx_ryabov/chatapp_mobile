import React, { Component } from 'react';
import { Animated, Easing, StyleSheet, TextInput, View, StyleProp, TextStyle, processColor, KeyboardTypeOptions } from 'react-native';
//import {Easing} from 'react-native-reanimated';

interface props {
    onChangeInputHandler: () => void;
    placeholder: string;
    value: string;
    secureTextEntry?: boolean;
}

interface state {
    isFocused: boolean;
}

class AnimatedInput extends Component<props, state> {
    state = {
        isFocused: false
    }

    _textPosition: Animated.Value = new Animated.Value(0);

    componentDidUpdate() {
        Animated.timing(this._textPosition, {
            toValue: (this.state.isFocused || this.props.value !== "") ? 1: 0,
            duration: 200,
            easing: Easing.inOut(Easing.ease)
        }).start();
    }

    onFocus = () => this.setState({isFocused: true});

    onBlur = () => this.setState({isFocused: false}); 

    render() {
        return (
            <View style={styles.container}>
                <Animated.Text style={{
                    position: "absolute",
                    top: this._textPosition.interpolate({
                        inputRange: [0, 1],
                        outputRange: [13 , -20]
                    }),
                    fontSize: this._textPosition.interpolate({
                        inputRange: [0, 1],
                        outputRange: [18, 12]
                    }),
                    color: "#98A1A5",
                    left: 15
                }}>{this.props.placeholder}</Animated.Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={this.props.onChangeInputHandler}
                    onFocus={this.onFocus.bind(this)}
                    onBlur={this.onBlur.bind(this)}
                    secureTextEntry={this.props.secureTextEntry}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        position: "relative",
        height: 50,
        marginTop: 30
    },
    textInput: {
        borderRadius: 5,
        paddingLeft: 15,
        height: "100%",
        backgroundColor: "rgba(0,0,0,.05)",
        borderTopLeftRadius: 5,
        color: "#3A404A"
    }    
});

export default AnimatedInput;
