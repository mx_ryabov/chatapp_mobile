import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Dimensions } from 'react-native';

const {width, height} = Dimensions.get('window');

type Props = {
    time: string;
    contact_name: string;
    last_message: { me: boolean; text: string; };
    photo?: string;
    isActive: boolean;
    onPress: () => void;
}

const ItemChatList = (props: Props) => {
    return (
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={styles.container}>
                <View style={styles.photo}>
                    <View style={styles.status}></View>
                </View>
                <View style={styles.content}>
                    <View style={{flexDirection: "column"}}>
                        <Text style={styles.mainText}>{props.contact_name}</Text>
                        <Text numberOfLines={2} style={styles.subText}>
                            {
                                props.last_message.text.length > 26
                                ? props.last_message.text.substring(0, 26) + '...'
                                : props.last_message.text
                            }
                        </Text>
                    </View>
                    <View>
                        <Text style={styles.subText}>{props.time}</Text>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
        
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        paddingVertical: 20,
        borderBottomColor: "#c4c3cb",
        borderBottomWidth: .5,
        flexDirection: "row"
    },
    photo: {
        width: 60,
        height: 60,
        borderRadius: 60,
        backgroundColor: "#c4c3cb",
        position: "relative",
        marginRight: 20
    },
    status: {

    },
    content: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        flexGrow: 1
    },
    mainText: {
        fontSize: 18,
        color: "#262626",
        fontWeight: "500"
    },
    subText: {
        fontSize: 15,
        color: "#c4c3cb",
    }
});

export default ItemChatList;
