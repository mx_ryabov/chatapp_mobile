import React, { Component } from 'react';
import { View, StyleSheet, TouchableHighlight, TouchableWithoutFeedback } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import AnimatedButton from './AnimatedButton';


interface props {
    isChat: boolean;
    pressLogout: () => void
}
class HeaderChat extends Component<props> {

    render() {
        return (
            <View style={styles.container}>
                {this.props.children}
                <View style={styles.logoutIcon}>
                    <AnimatedButton 
                        onPress={() => {}} 
                        type="text"
                        title={
                            <MaterialCommunityIcons name="logout" size={24} color="#c4c3cb" />
                        }
                        rippleColor="#000"
                    />
                </View>
                
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingTop: 30,
        backgroundColor: '#ffffff',
        height: 90,
        elevation: 1,
        flexDirection: "row",
        justifyContent: "center",
        position: "relative"
    },
    logoutIcon: {
        position: "absolute",
        right: 0,
        top: 37
    }
});


export default HeaderChat;
