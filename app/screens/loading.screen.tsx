import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { NavigationScreenProps } from 'react-navigation';



class LoadingSreen extends Component<NavigationScreenProps> {
    componentDidMount() {   
        AsyncStorage.getItem("token").then((val) => {
            if (val) {
                this.props.navigation.navigate("HomeStack");
            } else {
                this.props.navigation.navigate("LoginScreen");
            }
        });
    }
    render() {        
        return (
            <View style={styles.container}>
                <Text>Loading...</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});


export default LoadingSreen;
