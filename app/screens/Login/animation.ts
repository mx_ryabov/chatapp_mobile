import Animated, { Easing } from 'react-native-reanimated';
import {State} from 'react-native-gesture-handler';
import { Component } from 'react';
import { height } from './styles';

export const {
    Value, concat, interpolate, Extrapolate, 
    event, block, cond, eq, set, Clock, clockRunning, 
    startClock, timing, debug, stopClock
} = Animated;

function runTiming(clock:Animated.Clock, value:Animated.Adaptable<number>, dest:Animated.Adaptable<number>) {
    const state = {
      finished: new Value(0),
      position: new Value(0),
      time: new Value(0),
      frameTime: new Value(0)
    };
  
    const config = {
      duration: 1000,
      toValue: new Value(0),
      easing: Easing.inOut(Easing.ease)
    };
  
    return block([
      cond(clockRunning(clock), 0, [
        set(state.finished, 0),
        set(state.time, 0),
        set(state.position, value),
        set(state.frameTime, 0),
        set(config.toValue, dest),
        startClock(clock)
      ]),
      timing(clock, state, config),
      cond(state.finished, debug('stop clock', stopClock(clock))),
      state.position
    ]);
}


class AnmimateBaseClass<P, S> extends Component<P, S> {
    buttonOpacity: Animated.Value<number> = new Value(1);

    onStateChange = event([
        {
            nativeEvent: ({ state }) => 
                block([
                    cond(
                        eq(state, State.END),
                        set(this.buttonOpacity, runTiming(new Clock(), 1, 0))
                    )
                ])
        }
    ]);

    onCloseState = event([
        {
            nativeEvent: ({ state }) =>
                block([
                    cond(
                        eq(state, State.END),
                        set(this.buttonOpacity, runTiming(new Clock(), 0, 1))
                    )
                ])
        }
    ]);

    buttonY = interpolate(this.buttonOpacity, {
        inputRange: [0, 1],
        outputRange: [100, 0],
        extrapolate: Extrapolate.CLAMP
    });
  
    bgY = interpolate(this.buttonOpacity, {
        inputRange: [0, 1],
        outputRange: [-height / 3 - 25, 0],
        extrapolate: Extrapolate.CLAMP
    });

    textInputZindex = interpolate(this.buttonOpacity, {
        inputRange: [0, 1],
        outputRange: [1, -1],
        extrapolate: Extrapolate.CLAMP
    });

    textInputOpacity = interpolate(this.buttonOpacity, {
        inputRange: [0, 1],
        outputRange: [1, 0],
        extrapolate: Extrapolate.CLAMP
    });

    textInputY = interpolate(this.buttonOpacity, {
        inputRange: [0, 1],
        outputRange: [0, 100],
        extrapolate: Extrapolate.CLAMP
    });

    rotateCross = interpolate(this.buttonOpacity, {
        inputRange: [0, 1],
        outputRange: [180, 360],
        extrapolate: Extrapolate.CLAMP
    });
}


export default AnmimateBaseClass;
