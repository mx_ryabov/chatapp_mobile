import { StyleSheet, Dimensions } from "react-native";
export const {width, height} = Dimensions.get('window');


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F1F1F3",
        position: "relative"
    },
    mediaPanel: {
        position: "absolute",
        left: 0,
        width: "100%",
        bottom: 50,
        backgroundColor: "#fff",
        borderBottomWidth: 0.5,
        borderColor: "#AEBFCE",
        height: 90,
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    photoContainer: {
        position: "relative",
        width: 100,
    },
    photoMediaPanel: {
        borderRadius: 10,
        height: 70,
        width: 100,
        zIndex: 5
    },
    closePhotoBtn: {
        width: 20,
        height: 20,
        borderRadius: 10,
        elevation: 1,
        backgroundColor: "#AEBFCE",
        zIndex: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    typingPanel: {
        position: "absolute",
        right: 0,
        bottom: 0,
        width: "100%",
        height: 50,
        backgroundColor: "#fff",
        elevation: 1,
        flexDirection: "row",
        padding: 0
    },
    typingInput: {
        maxWidth: width - 130,
        flexGrow: 1
    },
    photo: {
        width: 36,
        height: 36,
        borderRadius: 18,
        elevation: 1,
        backgroundColor: "#AEBFCE",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        left: 20,
        top: 43
    },
    textPhoto: {
        color: "#fff",
        fontSize: 18
    },
    headerNameContainer: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    headerName: {
        fontSize: 20,
        color: "#3A404A"
    },
    headerNameStatus: {
        fontSize: 12,
        color: "#AEBFCE"
    },
    messageContainer: {
        flexDirection: "row",
        paddingHorizontal: 25,
        paddingVertical: 7
    },
    messageBox: {
        borderRadius: 15,
        paddingVertical: 10,
        paddingBottom: 20,
        paddingHorizontal: 15,
        flexDirection: "row",
        minWidth: "25%",
        maxWidth: "70%",
        position: "relative"
    },
    messageTime: {
        position: "absolute",
        fontSize: 10,
        bottom: 5,
        right: 10,
        color: "rgba(0,0,0,.3)"
    }
});