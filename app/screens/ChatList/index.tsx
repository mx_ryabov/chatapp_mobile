import React, { Component } from 'react';
import { View, Text, AsyncStorage, ScrollView, RefreshControl } from 'react-native';
import { styles } from './styles';
import HeaderChat from '../../components/HeaderChat';
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { NavigationStackProp } from 'react-navigation-stack';
import { UserState, IUser } from '../../store/user/types';
import ItemChatList from '../../components/ItemChatList';
import AnimatedButton from '../../components/AnimatedButton';
import { getChat } from '../../store/chat/actions';
import { ChatState, IChat } from '../../store/chat/types';
import { getUser } from '../../store/user/actions';


interface state {
    refreshing: boolean;
}

interface props {
    navigation: NavigationStackProp;
    user: UserState;
    chat: ChatState;
    getUser: () => void;
    getChat: (addressee_id?:string, chat_id?:string) => void;
}

class ChatListScreen extends Component<props, state> {
    static navigationOptions = {
        header: null
    }

    state = {
        refreshing: false
    }

    componentDidMount() {
        this.props.getChat();
        this.props.getUser();
    }

    goToChat(addressee) {
        this.props.navigation.push("ChatScreen", {addressee});
    }

    logout() {
        AsyncStorage.removeItem("token").then(() => {
            this.props.navigation.navigate("LoginScreen");
        });        
    }

    onRefresh() {
        this.props.getChat();
    }

    _renderList() {
        let chats:IChat[] = this.props.chat.chat_list;
        
        if (chats.length === 0) {
            return (
                <View style={styles.emtyStateContainer}>
                    <Text style={styles.emptyStateText}>Вы до сих пор ещё ни с кем не общались!</Text>
                    <AnimatedButton 
                        title="Выбрать собеседника" 
                        type="contained"
                        onPress={() => {this.props.navigation.navigate("ContactList")}} />
                </View>
                
            )
        } else {            
            return (
                <ScrollView style={{flex: 1}} refreshControl={
                    <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh.bind(this)} />
                }>
                    {chats.map((chat, key) => {
                        let {contact} = chat;                        
                        let last_message_text:string = chat.messages[0].text;
                        let date:string = new Date(chat.messages[0].date).toLocaleDateString();
                        
                        if (chat.messages[0].from === this.props.user.me._id) last_message_text = `Вы: ${last_message_text}`;
                        return (
                            <ItemChatList key={key}
                                        contact_name={`${contact.firstName}`}
                                        time={date}
                                        last_message={{me: false, text: last_message_text}}
                                        isActive={true}
                                        onPress={this.goToChat.bind(this, chat.contact)} />
                        )
                    })}
                </ScrollView>
            )
        }
    }

    render() {            
        return (
            <View style={styles.container}>
                <HeaderChat isChat={false} pressLogout={this.logout.bind(this)}>
                    <Text style={{color: "#4B4B50", fontWeight: "bold", fontSize: 22}}>Сообщения</Text>
                </HeaderChat>
                {this._renderList.bind(this)()}
            </View>
        );
    }
}



const mapStateToProps = (state: AppState) => ({
    user: state.user,
    chat: state.chat
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getChat: async (addressee_id?:string, chat_id?:string) => {
        dispatch(getChat(addressee_id, chat_id));
    },
    getUser: async () => {
        dispatch(getUser());
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(ChatListScreen);
