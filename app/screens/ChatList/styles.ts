import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F1F1F3",
        flexDirection: "column",
    },
    emtyStateContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        paddingHorizontal: 30
    },
    emptyStateText: {
        fontSize: 20,
        color: "#3A404A",
        marginBottom: 10,
        textAlign: "center"
    }
});