import React, { Component, ComponentState } from 'react';
import { View, Text, KeyboardAvoidingView, Alert } from 'react-native';
import { styles } from './styles';
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { NavigationStackProp } from 'react-navigation-stack';
import AnimatedInput from '../../components/AnimatedInput';
import { ScrollView } from 'react-native-gesture-handler';
import AnimatedButton from '../../components/AnimatedButton';
import { signUp } from '../../store/user/actions';
import { IUser, UserState } from '../../store/user/types';

const formFields = [
    {placeholder: "Имя", value: "firstName", secure: false},
    {placeholder: "Фамилия", value: "secondName", secure: false},
    {placeholder: "Логин", value: "login", secure: false},
    {placeholder: "Пароль", value: "password", secure: true },
    {placeholder: "Подтверждение пароля", value: "confirmPassword", secure: true}
]


interface state {
    login: string;
    password: string;
    confirmPassword: string;
    firstName: string;
    secondName: string;
}

interface props {
    navigation: NavigationStackProp;
    signUp: (
        login: string,
        password: string,
        firstName: string,
        secondName: string,
        done: (res) => void
    ) => void;
    user: UserState;
}

class ChatListScreen extends Component<props, state> {
    static navigationOptions = {
        header: null
    }

    state = {
        login: "",
        firstName: "",
        secondName: "",
        password: "",
        confirmPassword: ""
    }

    onChangeInputHandler(field:string, text:string) {
        this.setState({[field]: text} as ComponentState)
    }

    onRegisteredActionHandle(res) {
        if (res.success) {
            Alert.alert("Вы успешно зарегистрированы!");
            this.props.navigation.navigate("LoginScreen");
        }
    }

    signUp() {
        for (let field of formFields) {
            if (this.state[field.value].length === 0) {
                Alert.alert(`В поле ${field.placeholder} должен быть хотя бы один символ!`);
                return;
            }
        };
        if (this.state.password !== this.state.confirmPassword) {
            Alert.alert("Пароль и подтверждение пароля не совпадают!");
            return;
        }
        this.props.signUp(
            this.state.login,
            this.state.password,
            this.state.firstName,
            this.state.secondName,
            this.onRegisteredActionHandle.bind(this)
        );
    }


    render() {         
        return (
            <KeyboardAvoidingView behavior="padding" enabled style={styles.container}>
                <ScrollView style={{paddingHorizontal: 40, flex: 1}}>
                    <Text style={{fontSize: 28, color: "#3A404A", marginTop: 30}}>Регистрация</Text>
                    {formFields.map((field, ind) => {
                        return (
                            <AnimatedInput 
                                key={ind}
                                secureTextEntry={field.secure}
                                placeholder={field.placeholder}
                                value={this.state[field.value]}
                                onChangeInputHandler={this.onChangeInputHandler.bind(this, field.value)} />
                        )
                    })}
                    <View style={{marginTop: 30}}>
                        <AnimatedButton 
                            title="Зарегистрироваться" 
                            type="contained"
                            onPress={this.signUp.bind(this)} />
                        <View style={{marginTop: 10}}>
                            <AnimatedButton 
                                title="Вход" 
                                type="text"
                                rippleColor="#0078FF"
                                onPress={() => {this.props.navigation.navigate("LoginScreen")}} />
                        </View>
                        
                    </View>
                    
                </ScrollView>
                
            </KeyboardAvoidingView>
        );
    }
}



const mapStateToProps = (state: AppState) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    signUp: async (
        login: string,
        password: string,
        firstName: string,
        secondName: string,
        done: (res) => void
    ) => {
        dispatch(signUp(login, password, firstName, secondName, done));
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(ChatListScreen);
