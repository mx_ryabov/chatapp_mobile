import React, { Component } from 'react';
import { View, Text, ScrollView, AsyncStorage, RefreshControl } from 'react-native';
import {styles} from "./styles";
import { connect } from 'react-redux';
import { getUser } from '../../store/user/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../../store';
import { NavigationStackProp } from 'react-navigation-stack';
import { UserState, IUser } from '../../store/user/types';
import ItemContactList from '../../components/ItemContactList';
import HeaderChat from '../../components/HeaderChat';


interface state {
    refreshing: boolean;
}

interface props {
    navigation: NavigationStackProp;
    user: UserState;
    getUser: () => void;
}

class ContactList extends Component<props, state> {
    static navigationOptions = {
        header: null
    }

    state = {
        refreshing: false
    }

    componentDidMount() {
        this.props.getUser();
    }

    logout() {
        AsyncStorage.removeItem("token").then(() => {
            this.props.navigation.navigate("LoginScreen");
        });        
    }

    goToChat(addressee) {
        this.props.navigation.navigate("ChatScreen", {addressee});
    }

    _renderList() {
        const {contacts} = this.props.user;
        return contacts.map((contact:IUser, key) => {
            return (
                <ItemContactList 
                    key={key}
                    firstName={contact.firstName}
                    secondName={contact.secondName}
                    nickname={contact.login}
                    isActive={true} 
                    onPress={this.goToChat.bind(this, contact)}/>
            )
        })
    }

    onRefresh() {
        this.props.getUser();
    }

    render() {
        return (
            <View style={styles.container}>
                <HeaderChat 
                    isChat={false} 
                    pressLogout={this.logout.bind(this)}
                >
                    <Text style={styles.headerText}>Контакты</Text>
                </HeaderChat>
                <ScrollView refreshControl={
                    <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh.bind(this)} />
                }>
                    {this._renderList.bind(this)()}
                </ScrollView>
            </View>
        );
    }
}



const mapStateToProps = (state: AppState) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getUser: async () => {
        dispatch(getUser());
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(ContactList);
