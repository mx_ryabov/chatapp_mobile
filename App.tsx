import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { FontAwesome } from '@expo/vector-icons';

import ChatApp from './app/index';

function cacheImages(images:any) {
  return images.map((image:any) => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts:any) {
  return fonts.map((font:any) => Font.loadAsync(font));
}

type state = {isReady: boolean}
type props = {}

export default class App extends React.Component<props, state> {
  state = {
    isReady: false
  }

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require('./assets/bg1.jpg'),
    ]);

    const fontAssets = cacheFonts([FontAwesome.font]);

    await Promise.all([...imageAssets, ...fontAssets]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    
    return (
      <ChatApp/>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
