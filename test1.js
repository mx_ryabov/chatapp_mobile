const socketIOClient = require('socket.io-client');

let socket = socketIOClient("http://192.168.0.115:3000", {
    forceNew: true,
    query: 'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dpbiI6InRlc3QiLCJpYXQiOjE1NzU0NDUwNDd9.Pnzcsm3asLUkN34nQOU3B1OaUeWBLMt4chGqWbEDIp8'
});

let max = "5de280323d31da4d93281508";
let sasha = "5de79a736721383102156805";
let test = "5de280753d31da4d9328150a";

socket.emit('join', sasha, test);

socket.on('chat', (data) => {console.log(data);});
socket.on('private', (data) => {console.log('private data: ', data.map((el, ind) => {return `${ind}: ${el.text}`}))});